#################################################### Figure S1 B ####################################################
library(ggplot2)
library(XLConnect)

polyA = readWorksheetFromFile('Library_prep_comparison_RNA_viruses.xlsx', sheet=1)

level_order = c('Narnaviridae','Mitoviridae','Partitiviridae','Picobirnaviridae',
                'Nodaviridae','Botourmiaviridae','Virgaviridae','Amalgaviridae',
                'Totiviridae','Flaviviridae','Reovirales','Bunyavirales',
                'Leviviricetes','Astroviridae','Peribunyaviridae','Tombusviridae',
                'Aspiviridae','Chrysoviridae','Cystoviridae','Endornaviridae','Tymoviridae',
                'Picornavirales','Hypoviridae','Hepeviridae','Benyviridae', 
                'Togaviridae','Jingchuvirales','Rhabdoviridae','Orthomyxoviridae','Iflaviridae')

ggplot(polyA,aes(fill=Fraction, y=Value, x=factor(Family,level = level_order))) + 
  theme_bw() + 
  geom_bar(position="fill", stat="identity")+
  scale_fill_manual(values=c("#108690", "#FCCF14"))+
  labs(y = "Frequency (%) of virus contigs")+
  theme(axis.text.x = element_text(angle = 60, hjust = 1))


ggplot(polyA[1:30,], aes(x=factor(Family,level = level_order), y=Total)) + 
  theme_bw() + 
  geom_bar(position="stack", stat="identity")+
  labs(y = "Number of virus contigs")+
  theme(axis.text.x = element_text(angle = 60, hjust = 1))
