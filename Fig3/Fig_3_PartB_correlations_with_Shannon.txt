# load the required libraries
library("vegan")

# set the working directory
setwd("Your/directory")

##### RNA viral Macrodiversity

# Calculating Shannon's "H"
OTUs=read.table("adj_coverage_table_UNIQUE_tmean_90ID_75readcov_30contigcov_wcANI_90_80_1kb.csv",header=T,row.names=1,sep=",")
OTUs=t(OTUs)
OTUs=OTUs[-c(78,241,242, 511), ]
Shannon <- vegan::diversity(OTUs,"shannon")

# Reading the metadata table
Metadata= read.csv("Metadata_for_RNA_viral_Shannon.csv",header=T,row.names=1,sep=",")
Metadata=Metadata[-c(78,241,242, 511), ]

# Replacing pi in the metadata table with Shannon's H
Metadata$pi <- Shannon
Metadata=Metadata[c(581:767), ]

# Removing the alpha diversity outliers (019_SRF, 205_SUR)
Metadata = Metadata[-c(5,180), ]

# Preparing an output file for the calculated correlations
Metadata.t=t(Metadata)
output_table = data.frame(Metadata.t[,1])

# Calculating Pearson correlation between RNA virus Shannon and each of the environmental variables
for (i in c(1, 3: ncol(Metadata))){
  final = Metadata[,c(2,i)]
  final = final[complete.cases(final), ]
  final[1] <- lapply(final[1], as.numeric)
  Correlations <- cor(as.data.frame(final), use="pairwise.complete.obs", method="pearson")
  newcolumn = paste("pearson")
  output_table[i,newcolumn] <- Correlations[2,1]
  P_value = cor.test(as.numeric(final[,1]),as.numeric(final[,2]),alternative = "two.sided", method = "pearson", exact = NULL, conf.level = 0.95, continuity = FALSE)
  newcolumn2 = paste("pearson","P", sep = "_")
  output_table[i,newcolumn2] <- P_value$p.value
}

# Writing the corrleations and their P-values to an output file
output_table[1] = NULL
output_table = output_table[c(-1,-2,-3),]
write.table(output_table, "Correlations_with_Shannon_RNA_prok_pearson.txt",quote=F,sep="\t")

#-------------------------------------------------------------------------------------------------------------------------------------

##### DNA viral Macrodiversity
# Reading the metadata table
Metadata= read.csv("Metadata_with_DNA_viral_Shannon.csv",header=T,row.names=1,sep=",")
# Removing the alpha diversity outliers (32_DCM, 155_SUR, 56_MES, 70_MES, 72_MES, 102_MES)
Metadata = Metadata[-c(11,34,49,52,70,91), ]

# Preparing an output file for the calculated correlations
Metadata.t=t(Metadata)
output_table = data.frame(Metadata.t[,1])

# Calculating Pearson correlation between DNA virus Shannon and each of the environmental variables
for (i in c(4: ncol(Metadata))){
  final = Metadata[,c(2,i)]
  final = final[complete.cases(final), ]
  final[1] <- lapply(final[1], as.numeric)
  Correlations <- cor(as.data.frame(final), use="pairwise.complete.obs", method="pearson")
  newcolumn = paste("pearson")
  output_table[i,newcolumn] <- Correlations[2,1]
  P_value = cor.test(as.numeric(final[,1]),as.numeric(final[,2]),alternative = "two.sided", method = "pearson", exact = NULL, conf.level = 0.95, continuity = FALSE)
  newcolumn2 = paste("pearson","P", sep = "_")
  output_table[i,newcolumn2] <- P_value$p.value
}

# Writing the corrleations and their P-values to an output file
output_table[1] = NULL
output_table = output_table[c(-1,-2,-3),]
write.table(output_table, "Correlations_with_Shannon_DNA_pearson.txt",quote=F,sep="\t")


## to calculate spearman correlation tables, please re-run the entire code after replacing all "pearson" instances with "spearman"

