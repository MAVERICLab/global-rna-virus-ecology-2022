#################################################### Figure S3 (alpha boxblot) ####################################################
#### Shannon's H calculation ####
# read tables again since previous OTUs have empty samples removed
OTUs=read.table("adj_coverage_table_UNIQUE_tmean_90ID_75readcov_30contigcov_wcANI_90_80_1kb.csv",header=T,row.names=1,sep=",")

# Transpose the table
OTUs=t(OTUs)

# calculate shannon'H index
Shannon <- vegan::diversity(OTUs,"shannon")
write.table(Shannon, "Shannon.txt",quote=F,sep="\t")

#### Prokryotic fractions ####
# read shannon tables
group<-read.table("boxplot_group.txt", sep="\t", header=TRUE)

Shannon_boxplot_RNA_DNA <- group[-c(1:584), ]
Shannon_boxplot_RNA_DNA$x_ori <- gsub('0.22.1.6_T', '0.22to1.6', Shannon_boxplot_RNA_DNA$x_ori)
Shannon_boxplot_RNA_DNA$x_ori <- gsub('0.22.3_T', '0.22to3', Shannon_boxplot_RNA_DNA$x_ori)
Shannon_boxplot_RNA_DNA$x_ori <- gsub('148b_MES', '148_MES', Shannon_boxplot_RNA_DNA$x_ori)
Shannon_boxplot_RNA_DNA$x_ori <- gsub('MXL', 'MIX', Shannon_boxplot_RNA_DNA$x_ori)
Shannon_boxplot_RNA_DNA$x_ori <- gsub('168_IZZ', '168_MIX', Shannon_boxplot_RNA_DNA$x_ori)
Shannon_boxplot_RNA_DNA$x_ori <- gsub('175_IZZ', '175_MIX', Shannon_boxplot_RNA_DNA$x_ori)
Shannon_boxplot_RNA_DNA$x_ori <- gsub('180_ZZZ', '180_DCM', Shannon_boxplot_RNA_DNA$x_ori)
Shannon_boxplot_RNA_DNA$x_ori <- gsub('189_ZZZ', '189_MES', Shannon_boxplot_RNA_DNA$x_ori)

Shannon_boxplot_RNA_DNA <- Shannon_boxplot_RNA_DNA[, c(3:5)]
names(Shannon_boxplot_RNA_DNA)[names(Shannon_boxplot_RNA_DNA) == 'x_ori'] <- 'x'

Shannon_boxplot=read.table("Shannon.txt",header=T,sep="\t")

Shannon_boxplot_prok <- Shannon_boxplot[585:771,]
Shannon_boxplot_prok = merge(Shannon_boxplot_prok, Shannon_boxplot_RNA_DNA, by = "x", all.x = TRUE, all.y = FALSE)

# remove samples where shannon'H is 0
library(data.table)

Shannon_boxplot_prok = data.table(Shannon_boxplot_prok)
Shannon_boxplot_prok  <- Shannon_boxplot_prok[Shannon_boxplot_prok$Group1 != "Exclude"]
Shannon_boxplot_prok_filtered <- filter(Shannon_boxplot_prok, y != 0)

# assign colors to the groups
boxplot_colvec = c("#d5d5d5","#28b0d8","orange","#f7639b")

# plot the boxplot figure
ggplot(Shannon_boxplot_prok_filtered, aes(x=Group1, y=y ,color="black", fill= Group1,group=Group1)) +
  theme_bw() + stat_boxplot(geom ='errorbar', lwd=0.25) + 
  geom_boxplot(outlier.color="black", outlier.size = 0, lwd=0.25) +
  geom_jitter(width = .3, aes(fill = Group1), shape = 21, color = "black", size=0.8, alpha=0.2) + 
  theme(legend.position="none",plot.margin=unit(c(3,0.1,0,0.3),"cm"))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())+
  scale_color_manual(values="black")+ scale_fill_manual(values=boxplot_colvec)+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  labs(y = "Shannon's index (H)", x = "prok_filtered")+ ylim(0, 5)

MW1_pro_filtered = TukeyHSD(aov(Shannon_boxplot_prok_filtered$y ~ Shannon_boxplot_prok_filtered$Group1))


# plot the boxplot figure
boxplot_colvec2 = c("#28b0d8","#aedeec","#d5d5d5")

Shannon_boxplot_prok_filtered = Shannon_boxplot_prok_filtered[Shannon_boxplot_prok_filtered$Group2 != "Other"]

ggplot(Shannon_boxplot_prok_filtered, aes(x=Group2, y=y,color="black", fill= Group2,group=Group2)) +
  theme_bw() + stat_boxplot(geom ='errorbar', lwd=0.25) + 
  geom_boxplot(outlier.color="black", outlier.size = 0, lwd=0.25) +
  geom_jitter(width = .3, aes(fill = Group2), shape = 21, color = "black", size=0.8, alpha=0.2) + 
  theme(legend.position="none",plot.margin=unit(c(3,0.1,0,0.3),"cm"))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())+
  scale_color_manual(values="black")+ scale_fill_manual(values=boxplot_colvec2)+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  labs(y = "Shannon's index (H)", x = "prok_filtered")+ ylim(0, 5)

MW2_prok_filtered = TukeyHSD(aov(Shannon_boxplot_prok_filtered$y ~ Shannon_boxplot_prok_filtered$Group2))


#### Eukaryotic fractions #### 
# read shannon tables
group<-read.table("boxplot_group.txt", sep="\t", header=TRUE)
Shannon_boxplot_RNA_DNA <- group[c(1:584), ]

Shannon_boxplot=read.table("Shannon.txt",header=T,sep="\t")

Shannon_boxplot_euk <- Shannon_boxplot[1:584,]
Shannon_boxplot_euk = merge(Shannon_boxplot_euk, Shannon_boxplot_RNA_DNA[, c(1,4:5)], by = "x", all.x = TRUE, all.y = FALSE)

# remove samples where shannon'H is 0
Shannon_boxplot_euk = data.table(Shannon_boxplot_euk)

Shannon_boxplot_euk  <- Shannon_boxplot_euk [Shannon_boxplot_euk$Group1 != "Exclude"]
Shannon_boxplot_euk_filtered <- filter(Shannon_boxplot_euk, y != 0)

# assign colors to the groups
boxplot_colvec = c("#d5d5d5","#28b0d8","orange","#f7639b")

# plot the boxplot figure 
ggplot(Shannon_boxplot_euk_filtered, aes(x=Group1, y=y ,color="black", fill= Group1,group=Group1)) +
  theme_bw() + stat_boxplot(geom ='errorbar', lwd=0.25) + 
  geom_boxplot(outlier.color="black", outlier.size = 0, lwd=0.25) +
  geom_jitter(width = .3, aes(fill = Group1), shape = 21, color = "black", size=0.8, alpha=0.2) + 
  theme(legend.position="none",plot.margin=unit(c(3,0.1,0,0.3),"cm"))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())+
  scale_color_manual(values="black")+ scale_fill_manual(values=boxplot_colvec)+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  labs(y = "Shannon's index (H)", x = "euk_filtered")+ ylim(0, 5)

MW1_euk_filtered = TukeyHSD(aov(Shannon_boxplot_euk_filtered$y ~ Shannon_boxplot_euk_filtered$Group1))


# plot the boxplot figure
boxplot_colvec2 = c("#28b0d8","#aedeec","#d5d5d5")

Shannon_boxplot_euk_filtered = Shannon_boxplot_euk_filtered[Shannon_boxplot_euk_filtered$Group2 != "Other"]

ggplot(Shannon_boxplot_euk_filtered, aes(x=Group2, y=y,color="black", fill= Group2,group=Group2)) +
  theme_bw() + stat_boxplot(geom ='errorbar', lwd=0.25) + 
  geom_boxplot(outlier.color="black", outlier.size = 0, lwd=0.25) +
  geom_jitter(width = .3, aes(fill = Group2), shape = 21, color = "black", size=0.8, alpha=0.2) + 
  theme(legend.position="none",plot.margin=unit(c(3,0.1,0,0.3),"cm"))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())+
  scale_color_manual(values="black")+ scale_fill_manual(values=boxplot_colvec2)+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  labs(y = "Shannon's index (H)", x = "euk_filtered")+ ylim(0, 5)

MW2_euk_filtered = TukeyHSD(aov(Shannon_boxplot_euk_filtered$y ~ Shannon_boxplot_euk_filtered$Group2))


